from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from datetime import datetime, timedelta
from dateutil import tz, parser
from tutorial.auth_helper import get_sign_in_flow, get_token_from_code, store_user, remove_user_and_token, get_token
from tutorial.graph_helper import *

# <HomeViewSnippet>
def home(request):
  context = initialize_context(request)

  return render(request, 'tutorial/home.html', context)
# </HomeViewSnippet>

# <InitializeContextSnippet>
def initialize_context(request):
  context = {}

  # Check for any errors in the session
  error = request.session.pop('flash_error', None)

  if error != None:
    context['errors'] = []
    context['errors'].append(error)

  # Check for user in the session
  context['user'] = request.session.get('user', {'is_authenticated': False})
  return context
# </InitializeContextSnippet>

# <SignInViewSnippet>
def sign_in(request):
  # Get the sign-in flow
  flow = get_sign_in_flow()
  # Save the expected flow so we can use it in the callback
  try:
    request.session['auth_flow'] = flow
  except Exception as e:
    print(e)
  # Redirect to the Azure sign-in page
  return HttpResponseRedirect(flow['auth_uri'])
# </SignInViewSnippet>

# <SignOutViewSnippet>
def sign_out(request):
  # Clear out the user and token
  remove_user_and_token(request)

  return HttpResponseRedirect(reverse('home'))
# </SignOutViewSnippet>

# <CallbackViewSnippet>
def callback(request):
  # Make the token request
  result = get_token_from_code(request)

  #Get the user's profile
  user = get_user(result['access_token'])

  # Store user
  store_user(request, user)
  return HttpResponseRedirect(reverse('home'))
# </CallbackViewSnippet>

def emails(request):
  context = initialize_context(request)
  user = context['user']

  token = get_token(request)

  events = get_emails(token)

  if events:

    context['events'] = events['value']

  return render(request, 'tutorial/emails.html', context)

def new_email(request):
  context = initialize_context(request)
  user = context['user']

  if request.method == 'POST':
    # Validate the form values
    # Required values
    if (not request.POST['ev-subject']) or \
       (not request.POST['ev-recipients']):
      context['errors'] = [
        { 'message': 'Invalid values', 'debug': 'The subject, and recipients fields are required.'}
      ]
      return render(request, 'tutorial/newemail.html', context)

    recipients = [x.strip() for x in request.POST['ev-recipients'].split(';')]

    # Create the event
    token = get_token(request)

    new_email_id = create_email(
      token,
      request.POST['ev-subject'],
      recipients,
      request.POST['ev-body'])
    send_email(token, new_email_id)

    # Redirect back to emails view
    return HttpResponseRedirect(reverse('emails'))
  else:
    # Render the form
    return render(request, 'tutorial/newemail.html', context)
