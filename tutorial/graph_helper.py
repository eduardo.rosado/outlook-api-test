# Copyright (c) Microsoft Corporation.
# Licensed under the MIT License.

# <FirstCodeSnippet>
import requests
import json

graph_url = 'https://graph.microsoft.com/v1.0'

def get_user(token):
  # Send GET to /me
  user = requests.get(
    '{0}/me'.format(graph_url),
    headers={
      'Authorization': 'Bearer {0}'.format(token)
    },
    params={
      '$select': 'displayName,mail,mailboxSettings,userPrincipalName'
    })
  # Return the JSON result
  return user.json()
# </FirstCodeSnippet>


def get_emails(token):
  # Set headers
  headers = {
    'Authorization': 'Bearer {0}'.format(token),
  }

  # Configure query parameters to
  # modify the results
  query_params = {
    '$select': 'sender,subject',
    '$top': '10'
  }

  # Send GET to /me/messages
  events = requests.get('{0}/me/messages'.format(graph_url),
    headers=headers,
    params=query_params)

  # Return the JSON result
  return events.json()


def create_email(token, subject, recipients, body=None):
  # Create an email object
  new_email = {
    "subject":subject,
    "toRecipients":[{"emailAddress":{"address":email}} for email in recipients],
  }

  if body:
    # Create an itemBody object
    # https://docs.microsoft.com/graph/api/resources/itembody?view=graph-rest-1.0
    new_email['body'] = {
      'contentType': 'text',
      'content': body
    }

  # Set headers
  headers = {
    'Authorization': 'Bearer {0}'.format(token),
    'Content-Type': 'application/json'
  }

  response = requests.post('{0}/me/messages'.format(graph_url),
    headers=headers,
    data=json.dumps(new_email))

  if response.status_code != 201:
    raise Exception("Error creating the message")

  return response.json()["id"]

def send_email(token, id):

  headers = {
    'Authorization': 'Bearer {0}'.format(token),
    'Content-Type': 'application/json'
  }

  response = requests.post('{0}/me/messages/{1}/send'.format(graph_url, id),
    headers=headers)

  return response